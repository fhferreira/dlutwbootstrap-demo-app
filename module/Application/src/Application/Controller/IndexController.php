<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction() {
        return $this->redirect()->toRoute('dlu_twb_demo_demo');
    }
}
