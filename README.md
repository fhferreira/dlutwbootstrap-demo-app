DluTwBootstrap Demo App (ZF2 application)
=========================================

-------------------------------------------------

Introduction
------------
The DluTwBootstrap Demo App is a ZF2 application demonstratiting the capabilities of the [DluTwBootstrap module](https://bitbucket.org/dlu/dlutwbootstrap)
using the [DluTwBootstrap Demo module](https://bitbucket.org/dlu/dlutwbootstrap-demo).
Please go to [http://apps.zfdaily.com/dlutwbootstrap-demo](http://apps.zfdaily.com/dlutwbootstrap-demo) to see this application live on-line.

Who is this for?
----------------
Normally you should not need to install this application on your development machine. If you want to use Twitter Bootstrap in your ZF2 application,
just install the [DluTwBootstrap module](https://bitbucket.org/dlu/dlutwbootstrap) into your project and optionally also install the
[DluTwBootstrap Demo module](https://bitbucket.org/dlu/dlutwbootstrap-demo) to check that everything is working properly.
This application merely wires the two modules into the standard [ZF2 Skeleton Application](https://github.com/zendframework/ZendSkeletonApplication). It may be useful to you though, if you are having problems installing those modules or you want
to see them deployed in an actual application.

--------------------------------------------------------------

Installation using Composer
---------------------------

1.   `cd my/project/dir`
2.   `git clone https://bitbucket.org/dlu/dlutwbootstrap-demo-app.git`
3.   `cd dlutwbootstrap-demo-app`
4.   `php composer.phar install`
5.   Copy everything from `dlutwbootstrap-demo-app/vendor/dlu/dlutwbootstrap/public` to `dlutwbootstrap-demo-app/public`
6.   Create a vhost or a virtual directory on your web server and point its root to the `dlutwbootstrap-demo-app/public` directory.
7.   Turn URL rewriting on (follow the docs for your web server).
8.   If you are on an Apache web server, you might want to base your `.htaccess` on the template available in `dlutwbootstrap-demo-app/public/.htaccess.distr`.

Check and Demo
--------------

Go to the index of your new web application. It should redirect to the DluTwBootstrap demo.

-----------------------------------------------------------------------------------

Links
-----

- [DluTwBootstrap (ZF2 module) source](https://bitbucket.org/dlu/dlutwbootstrap)
- [DluTwBootstrap Demo (ZF2 module) source](https://bitbucket.org/dlu/dlutwbootstrap-demo)
- DluTwBootstrap Demo App (ZF2 application)
    - [Live Demo App @ http://apps.zfdaily.com/dlutwbootstrap-demo](http://apps.zfdaily.com/dlutwbootstrap-demo)
    - [Source](https://bitbucket.org/dlu/dlutwbootstrap-demo-app)
- [Tutorials and discussion of DluTwBootstrap on ZF Daily](http://www.zfdaily.com/tag/dlutwbootstrap/)